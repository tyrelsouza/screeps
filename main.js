var structureTower = require('structure.tower');
var creepsConfig = require('creeps.config');
var helpers = require('helpers');
var creepRolePriority = ['harvester', 'builder', 'upgrader', 'guard'];


module.exports.loop = function(){
    console.log(' ');
    // Cleanup Creeps
    for (var name in Memory.creeps){
        if (!Game.creeps[name]){
            delete Memory.creeps[name];
            console.log('Cleaning non-existing creep memory:', name);
        }
    }

    // Refactor some similar behavior for all roles
    for (var i in creepRolePriority){
        var roleName = creepRolePriority[i];
        var defaults = creepsConfig[roleName];

        let creepsOfKind = _.filter(Game.creeps, (creep) => {return creep.memory.role == roleName});
        if (creepsOfKind.length < defaults.minimumCreeps){
            var newName = Game.spawns.Spawn1.createCreep(defaults.bodyParts,
                                                         helpers.generateName(roleName), // Make names show the roles. Change me to undefined if this gets annoying.
                                                         {role: roleName});
            if(newName == ERR_NOT_ENOUGH_ENERGY){
                console.log('not enough resources to spawn');
            } else {
                console.log('Spawning', newName);
            }
        }
    }
    
    var tower = Game.getObjectById('577683641404f96570a580d2');
    if(tower) {
       structureTower.run(tower);
    }

    // Run stuff for each creep
    var roleCounts = {harvester: 0, builder: 0, upgrader: 0, guard: 0};
    var expected = _(creepsConfig).mapValues('minimumCreeps');

    for (var name in Game.creeps){ 
        var creep = Game.creeps[name];
        var role = creep.memory.role;
        roleCounts[role] += 1;
        creepsConfig[role].role.run(creep);
    }
    console.log('current\t', JSON.stringify(roleCounts));
    console.log('expect\t', JSON.stringify(expected));
    console.log('#############################################################');
};
