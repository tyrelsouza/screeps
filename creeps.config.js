var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleGuard = require('role.guard');

module.exports = {
    harvester: {
        bodyParts: [WORK,WORK, CARRY,CARRY, MOVE, MOVE],
        minimumCreeps: 4,
        role: roleHarvester
    },
    builder: {
        bodyParts: [WORK, CARRY, CARRY, CARRY, MOVE, MOVE],
        minimumCreeps: 0,
        role: roleBuilder
    },
    upgrader: {
        bodyParts: [WORK, CARRY, WORK, MOVE,  CARRY, MOVE, MOVE],
        minimumCreeps: 4,
        role: roleUpgrader
    },
    guard: {
        bodyParts: [TOUGH, MOVE, ATTACK, MOVE, ATTACK],
        minimumCreeps: 0,
        role: roleGuard,
    }
};