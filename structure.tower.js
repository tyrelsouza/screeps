var battle = require('battle')
var helpers = require('helpers');

var structureTower= {
    run: function(structure){
        this.structure = structure;
        console.log(structure, 'running Tower')

    	var target = battle.findEnemy(this.structure);

    	if(target != structure.room.controller) {
            console.log('\t', this.structure, 'attacking:',target)
    		this.structure.attack(target);
    	} else {
           var damagedStructure = helpers.findDamagedStructure(this.structure);
           if(damagedStructure){
               console.log("repairing", damagedStructure);
               this.structure.repair(damagedStructure);
           }
    	}
    }
}
module.exports = structureTower;
