var battle = require('battle')
var helpers = require('helpers');

var roleGuard = {
    getDestinationFlag: function(){
        return Game.flags['patrol_' + this.creep.memory.patrolDestination];
    },
    patrol: function(){
        if (this.creep.pos.isEqualTo(this.getDestinationFlag())){
            this.creep.memory.patrolDestination += 1;
        }
        if (this.creep.memory.patrolDestination > this.lastFlag){
            this.creep.memory.patrolDestination = 1;
        }
        this.creep.moveTo(this.getDestinationFlag());
    },
    run: function(creep){
        this.creep = creep;
        this.lastFlag = helpers.getLastFlag("patrol_");
        creep.memory.patrolDestination = creep.memory.patrolDestination || 1;
    	var target = battle.findEnemy(creep);
    	if(target != creep.room.controller && creep.hits > creep.hitsMax - 500 /* no more attack */) {
            console.log('\t', creep, 'attacking:',target)
    		creep.moveTo(target);
    		creep.attack(target);
    	} else {
            if (this.lastFlag > 0){
                this.patrol();
            } else {
                this.creep.moveTo(creep.room.controller)
            }
    	}

    }
}
module.exports = roleGuard;